# Personal Configurations

Files for personal configuration files and folders for customisation.

The terminal configuration uses `zsh` and is based on
[Maxim Danilov's article Make Linux terminal great again](https://maxim-danilov.github.io/make-linux-terminal-great-again/).

## Content

- `.gitignore`: Ignores all files (except README) to not have massive diffs
- `.config/`
  - `git/`
    - `gitconfig/`
      - `aliases.gitconfig`: Custom git aliases
      - `core.gitconfig`: Core configuration
      - `init.gitconfig`: Default init config (use `main` branch)
  - `termintor/`
    - `config`: Terminator configuration using
      [penumbra](https://github.com/nealmckee/penumbra) color theme, and
      JetBrainsMono font
  - `zsh/`
    - `zprofile/`
      - `ubuntu.zsh`: Personal ubuntu zprofile
      - `macos.zsh`: Personal macos zprofile
    - `zshrc/`
      - `aliases.zsh`: Setup personal aliases
      - `terminal.zsh`: Setup terminal with oh my zsh with plugins
- `.local/`
  - `share/`
    - `fonts/`
      - `JetBrainsMono/`: The
        [JetBrains Mono Font](https://www.jetbrains.com/lp/mono/) for use in
        Ubuntu

## Installation

### Initialise repository in home directory

Open a terminal and navigate to the home directory.

```zsh
cd ~
```

Initialise a new git repository in the home directory. (Delete any existing
repository first with `rm -rm .git`)

```zsh
git init
```

Add remote connection to the GitLab repository.

```zsh
git remote add origin git@gitlab.com:NiklasBurggraaff/personal-configurations.git
```

### Checkout remote git branch

Checkout the `main` branch.

```zsh
git pull
git checkout main
```

If this succeeds without errors continue to the
[next step](#setup-configuration).

The following error will appear if any of the files in this repository already
exist, which is likely:

```zsh
error: The following untracked working tree files would be overwritten by checkout:
	<files>
Please move or remove them before you switch branches.
Aborting
```

To fix the error move all of the files listed into a temporary folder. First
create a temporary folder using `mkdir temp`. The for each file listed in the
error, move it into the temporary folder.

```zsh
mv <file> temp/<file>
```

After this attempt to checkout the `main` branch again, until it is successful,
moving additional files as necessary.

```zsh
git checkout main
```

Move all the files back from the temporary folder to the home directory.

```zsh
mv -v temp/\.?* .
```

(The `\.?` is a wildcard for hidden files.)

### Setup configurations

#### `.zshrc`

In order to setup `zsh` with this personal configuration, source the files in
`.config/zsh/zshrc/` at the top of the `.zshrc` file as required. For example:

```zsh
source $HOME/.config/zsh/zshrc/aliases.zsh
```

Look at the contents of those files to determine what can be removed from the
`.zshrc` file, to not have duplicate execution.

#### `.zprofile`

In order to setup `zprofile` with this personal configuration, source the
correct file in `.config/zsh/profile/`, based on the operating system, at the
top of the `.profile` file as required. For example:

```zsh
source $HOME/.config/zsh/zprofile/ubuntu.zsh
```

Make sure to rename the `.profile` file to `.zprofile` to be used in `zsh`:

```zsh
mv ~/.profile ~/.zprofile
```

#### `.gitconfig`

In order to setup `git` with this personal configuration, the files in
`.config/git/gitconfig/` can be included in the `.gitconfig` file as required.
This can be done using
[includes](https://git-scm.com/docs/git-config#_includes). For example:

```gitconfig
[include]
		path = .config/git/gitconfig/aliases.gitconfig
```

The first thing you should do when you install Git is to set your user name and
email address. This is important because every Git commit uses this information,
and it’s immutably baked into the commits you start creating. This is something
not in this repo as this is different for everybody, so run the following
commands, entering the correct details:

```zsh
git config --global user.name "Lorem Ipsum"
git config --global user.email lorem@ipsum.com
```

#### Fonts

For MacOS, the contents of the `.local/share/fonts/JetBrainsMono/` folder should
be copied to `Library/Fonts/`. This can be done using the following command:

```zsh
cp -a $HOME/.local/share/fonts/JetBrainsMono/. $HOME/Library/Fonts/
```

#### Other

For all other files look at the `git diff` to determine what changes need to be
made to the files.

## Contributing

The `.gitignore` file excludes all files except this README. This means that any
changes made in the home directory, excluding changes to files in the
repository, will NOT appear in the `git diff`.

In order to add any files to the repository, use `git add -f <file>` to force
add the file to the repository.

If there are any computer specific settings these should not be pushed to the
repository.

## License

Copyright 2022 Niklas Burggraaff

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
